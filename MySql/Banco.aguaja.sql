	create table tbclientes(
	iduser int primary key auto_increment, 
	nome varchar(50) not null,
	email varchar(50) not null unique,
	senha  varchar(15) not null,
	endereco varchar(150),
	cpf varchar(20) unique,
	dataNascimento date
	);
   
	

create table tbvendedores(
	idven int primary key auto_increment, 
	nome varchar(50) not null,
	email varchar(50) not null unique,
	senha  varchar(15) not null,
	endereco varchar(150),
	cpf varchar(20) unique,
	dataNascimento date
	);
    
       create table tbestoque(
        idest int primary key auto_increment,
        marca varchar(30) not null,
        litros int not null,
        preco decimal(10,2) not null,
        quantidade int not null,
        idven int not null,
        foreign key (idven) references tbvendedores(idven)
        );

ALTER TABLE tbvendedores ADD estabelecimento varchar(50) not null;

create table tbpedidos(
idped int primary key auto_increment,
pedido varchar(255),
valorTotal decimal(10,2),
idven int not null,
iduser int not null,
situacao int not null,
foreign key (iduser) references tbclientes(iduser),
foreign key (idven) references tbvendedores(idven)
);

 create table tbdetalhespedido(
        idped int not null,
        idest int not null,
		quantidade int not null,
        idven int not null,
        iduser int not null,
        foreign key (idest) references tbestoque(idest),
        foreign key (idped) references tbpedidos(idped),
        foreign key (iduser) references tbclientes(iduser),
        foreign key (idven) references tbvendedores(idven)
        );	