
package controller;

import model.VendedorModel;
import entidades.VendedorDto;
import contratos.IUsuarioService;
import java.util.ArrayList;


public class VendedorController {
    
    private IUsuarioService usuariocont;
    
    public VendedorController() {
        this.usuariocont = new VendedorModel();
    }
    
    public boolean GravaUser(VendedorDto usr){
        return this.usuariocont.Adicionar(usr);
    }
    
    public boolean AlteraUser(VendedorDto usr){
        return this.usuariocont.Alterar(usr);
    }
    
    public boolean DeletarUser(VendedorDto usr){
        return this.usuariocont.Deletar(usr);
    }
    
    public boolean Logar(String email, String senha){
        return this.usuariocont.logar(email, senha);
    }
    public ArrayList ListarEstabelecimentos(VendedorDto usr){
        return this.usuariocont.ListarEstabelecimentos(usr);
    }
    public boolean AdicionarEstabelecimento(VendedorDto usr){
        return this.usuariocont.AdicionarEstabelecimento(usr);
    }
     public boolean validaIdade(java.sql.Date dataN)
     {
         return this.usuariocont.validaIdade(dataN);
     }
}
