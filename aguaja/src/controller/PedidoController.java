/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import contratos.IPedidoService;
import entidades.CompradorDto;
import entidades.PedidoDto;
import entidades.UsuarioDto;
import entidades.VendedorDto;
import java.util.ArrayList;
import model.PedidoModel;

/**
 *
 * @author Mateus
 */
public class PedidoController {
    
    private IPedidoService pedido;
    
    public PedidoController(){
        
        this.pedido = new PedidoModel();
    }
    
     public boolean Adicionar(PedidoDto pedido){
         return this.pedido.Adicionar(pedido);
     }
     
    public boolean Alterar(int idPedido){
        return this.pedido.Alterar(idPedido);
        
    }
    
    public boolean Deletar(int idPedido){
        return this.pedido.Deletar(idPedido);
    }
     
     
     public ArrayList ListaTodosFiltrado(UsuarioDto usuario, int status, int tipo){
          return this.pedido.ListaTodosFiltrado(usuario, status, tipo);
      }
     public int ListaIdPedido(VendedorDto vendedor,CompradorDto usuario){
         return this.pedido.ListaIdPedido(vendedor, usuario);
     }
      
      public boolean ConfirmaRecebimento(CompradorDto comprador){
          return this.pedido.ConfirmaRecebimento(comprador);
      }
}
