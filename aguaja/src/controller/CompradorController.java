
package controller;

import model.CompradorModel;
import entidades.CompradorDto;
import contratos.IUsuarioService;
import java.util.ArrayList;


public class CompradorController {
    
    private IUsuarioService usuariocont;
    
    public CompradorController() {
        this.usuariocont = new CompradorModel();
    }
    
    public boolean GravaUser(CompradorDto usr){
        return this.usuariocont.Adicionar(usr);
    }
    
    public boolean AlteraUser(CompradorDto usr){
        return this.usuariocont.Alterar(usr);
    }
    
    public boolean DeletarUser(CompradorDto usr){
        return this.usuariocont.Deletar(usr);
    }
    
      public boolean Logar(String email, String senha){
        return this.usuariocont.logar(email, senha);
    }
       public ArrayList ListarEstabelecimentos(CompradorDto comp){
           return this.usuariocont.ListarEstabelecimentos(comp);
       }
       
      public boolean validaIdade(java.sql.Date data){
          return this.usuariocont.validaIdade(data);
      }
      
      public double valorFrete(String end1, String end2){
          return this.usuariocont.valorFrete(end1, end2);
      }
}
