/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import contratos.IEstoqueService;
import entidades.PedidoDto;
import entidades.ProdutoDto;
import entidades.VendedorDto;
import java.util.ArrayList;
import model.EstoqueModel;


public class EstoqueController {
    
    private IEstoqueService estoque;
    
    public EstoqueController(){
        this.estoque = new EstoqueModel();
    }
    
    public boolean Adicionar(ProdutoDto produto, VendedorDto vendedor){
        return this.estoque.Adicionar(produto, vendedor);
    }
    public boolean Alterar(ProdutoDto produto){
        return this.estoque.Alterar(produto);
    }
    public boolean Deletar(ProdutoDto produto){
        return this.estoque.Deletar(produto);
    }
    public ArrayList ListaTodos(){
        return this.estoque.ListaTodos();
    }
    public ArrayList ListaTodosFiltrado(VendedorDto vendedor){
        return this.estoque.ListaTodosFiltrado(vendedor);
    }
     public boolean BaixaEstoque(ProdutoDto produto){
         return this.estoque.BaixaEstoque(produto);
     }
}
