/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import contratos.IDetalhesPedidoService;
import entidades.PedidoDto;
import entidades.ProdutoDto;
import java.util.ArrayList;
import model.DetalhesPedidoModel;

/**
 *
 * @author Mateus
 */
public class DetalhesPedidoController {
    private IDetalhesPedidoService DetalhesPedido;
    
    public DetalhesPedidoController(){
        DetalhesPedido = new DetalhesPedidoModel();
    }
    public boolean Adicionar(PedidoDto pedido){
        return this.DetalhesPedido.Adicionar(pedido);
    }
    
    public boolean Remover(int idPedido){
        return this.DetalhesPedido.Remover(idPedido);
    }
    
     public ArrayList<ProdutoDto> Consultar(int idPedido){
         return this.Consultar(idPedido);
     }
    
}
