
package entidades;

import java.util.Date;


public abstract class UsuarioDto {
    
    private int idUsr;
    private String nome;
    private String email;
    private String senha;
    private String endereco; // poderia ser uma classe, porém para simplficar fizemos apenas como atributo
    private String cpf;
    private Date dataNascimento;

    public UsuarioDto() {
    }

    public UsuarioDto(int idUsr, String nome, String email, String senha, String endereco, String cpf, Date dataNascimento) {
        this.idUsr = idUsr;
        this.nome = nome;
        this.email = email;
        this.senha = senha;
        this.endereco = endereco;
        this.cpf = cpf;
        this.dataNascimento = dataNascimento;
    }

    

    public UsuarioDto(int idUsr) {
        this.idUsr = idUsr;
    }
    
    
     public int getIdUsr() {
        return idUsr;
    }

    public void setIdUsr(int idUsr) {
        this.idUsr = idUsr;
    }
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
}
    