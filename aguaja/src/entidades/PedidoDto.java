/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author rafae
 */
public class PedidoDto {

    private int idcli;
    private int idvend;
    private int idpedido;
    private double valorTotal;
    private Date dataPedido;
    ArrayList<ProdutoDto> pedido;
    private int status;
    
    public PedidoDto() {
    }

    public PedidoDto(int idcli, int idvend, double valorTotal, ArrayList<ProdutoDto> pedido, int status) {
        this.idcli = idcli;
        this.idvend = idvend;
        this.valorTotal = valorTotal;
        this.pedido = pedido;
        this.status = status;
    }
    

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getIdcli() {
        return idcli;
    }

    public void setIdcli(int idcli) {
        this.idcli = idcli;
    }

    public int getIdvend() {
        return idvend;
    }

    public void setIdvend(int idvend) {
        this.idvend = idvend;
    }

    public int getIdpedido() {
        return idpedido;
    }

    public void setIdpedido(int idpedido) {
        this.idpedido = idpedido;
    }

    public double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Date getDataPedido() {
        return dataPedido;
    }

    public void setDataPedido(Date dataPedido) {
        this.dataPedido = dataPedido;
    }

    public ArrayList<ProdutoDto> getPedido() {
        return pedido;
    }

    public void setPedido(ArrayList<ProdutoDto> pedido) {
        this.pedido = pedido;
    }

}
