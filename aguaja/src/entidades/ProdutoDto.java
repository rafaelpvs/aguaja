package entidades;

import java.util.Date;


public class ProdutoDto {
    private int idproduto;
    private String marca;
    private double preco;
    private int litros;
    private double subTotal;
     private int quantidade;

    public ProdutoDto(int idproduto, int quantidade) {
        this.idproduto = idproduto;
        this.quantidade = quantidade;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public ProdutoDto() {
    }
  
    public ProdutoDto(String marca, double preco, int litros, int quantidade) {
        this.marca = marca;
        this.preco = preco;
        this.litros = litros;
        this.quantidade = quantidade;
    }

    public int getIdproduto() {
        return idproduto;
    }

    public void setIdproduto(int idproduto) {
        this.idproduto = idproduto;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public int getLitros() {
        return litros;
    }

    public void setLitros(int litros) {
        this.litros = litros;
    }
    
    

  
}
