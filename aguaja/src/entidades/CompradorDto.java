/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.util.Date;

/**
 *
 * @author rafae
 */
public class CompradorDto extends UsuarioDto{

    public CompradorDto() {
    }

    public CompradorDto(int idUsr, String nome, String email, String senha, String endereco, String cpf, Date dataNascimento) {
        super(idUsr, nome, email, senha, endereco, cpf, dataNascimento);
    }

    public CompradorDto(int idUsr) {
        super(idUsr);
    }
    
    
    
}
