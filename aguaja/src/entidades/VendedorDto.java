/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.util.Date;

/**
 *
 * @author rafae
 */
public class VendedorDto extends UsuarioDto{
    
    private String estabelecimento;

    public String getEstabelecimento() {
        return estabelecimento;
    }

    public void setEstabelecimento(String estabelecimento) {
        this.estabelecimento = estabelecimento;
    }

    public VendedorDto(int idUsr) {
        super(idUsr);
    }

    public VendedorDto() {
    }

    public VendedorDto(int idUsr, String nome, String email, String senha, String endereco, String cpf, Date dataNascimento) {
        super(idUsr, nome, email, senha, endereco, cpf, dataNascimento);
    }
    
    
}
