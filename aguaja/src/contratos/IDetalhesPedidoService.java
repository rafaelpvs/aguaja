/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contratos;

import entidades.PedidoDto;
import entidades.ProdutoDto;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mateus
 */
public interface IDetalhesPedidoService {
    
      public boolean Adicionar(PedidoDto pedido);
      public boolean Remover(int idPedido);
      public List<ProdutoDto> Consultar(int idPedido);
}
