
package contratos;

import entidades.PedidoDto;
import entidades.ProdutoDto;
import entidades.VendedorDto;
import java.util.ArrayList;


public interface IEstoqueService {
    
    boolean Adicionar(ProdutoDto prod, VendedorDto vend);
    boolean Alterar(ProdutoDto produto);
    boolean Deletar(ProdutoDto produto);
    ArrayList ListaTodos();
    ArrayList ListaTodosFiltrado(VendedorDto vendedor);
    boolean BaixaEstoque(ProdutoDto produto);
  
}

