package contratos;

import entidades.CompradorDto;
import entidades.PedidoDto;
import entidades.UsuarioDto;
import entidades.VendedorDto;
import java.util.ArrayList;


public interface IPedidoService {
    boolean Adicionar(PedidoDto pedido);
    boolean Alterar(int idPedido);
    boolean Deletar(int idPedido);
    ArrayList ListaTodos();
    ArrayList ListaTodosFiltrado(UsuarioDto usuario, int status, int tipo);
    int ListaIdPedido(VendedorDto vendedor, CompradorDto usuario);
    boolean ConfirmaRecebimento(CompradorDto comprador);
}