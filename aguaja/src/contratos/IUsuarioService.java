package contratos;

import entidades.UsuarioDto;
import entidades.VendedorDto;
import java.util.ArrayList;



public interface IUsuarioService {
    boolean Adicionar(UsuarioDto usuario);
    boolean Alterar(UsuarioDto usuario);
    boolean Deletar(UsuarioDto usuario);
    boolean logar(String email, String senha);
    public ArrayList ListarEstabelecimentos(UsuarioDto usuario);
    boolean AdicionarEstabelecimento(VendedorDto usr);
    boolean validaIdade(java.sql.Date dataN);
    public double valorFrete(String end1, String end2);
  
    
}