/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import contratos.IDetalhesPedidoService;
import entidades.PedidoDto;
import entidades.ProdutoDto;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mateus
 */
public class DetalhesPedidoModel extends DaoUtil implements IDetalhesPedidoService{
    
    
 
    public boolean Adicionar(PedidoDto pedido) {
        
        
            String sql = "INSERT INTO tbdetalhespedido ";
            sql += "(idped, idest, idven, iduser, quantidade)";
            sql += " VALUES ";
            sql += "(?, ?, ?, ?, ?)";
            int ret = 0;
            
        for (ProdutoDto pedido1 : pedido.getPedido()) {
            try {
                PreparedStatement ps = super.getPreparedStatement(sql);
                ps.setInt(1, pedido.getIdpedido());
                ps.setInt(2, pedido1.getIdproduto());
                ps.setInt(3, pedido.getIdvend());
                ps.setInt(4, pedido.getIdcli());
                ps.setInt(5, pedido1.getQuantidade());
                
                ret = ps.executeUpdate();
                ps.close();
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(PedidoModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

         return ret > 0;
    }
    
    public boolean Remover(int idPedido){
        String sql = "DELETE FROM tbdetalhespedido ";
        sql += " WHERE idped=?";

        int ret = 0;
        try {
            PreparedStatement ps = super.getPreparedStatement(sql);
            ps.setInt(1, idPedido);
            ret = ps.executeUpdate();
            ps.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(CompradorModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return ret > 0;
    }
    
    @Override
    public ArrayList Consultar(int idPedido){
        
        
         String sql = "SELECT idest, quantidade FROM tbdetalhespedido";
         sql+= " WHERE idped = " + idPedido;
         try {
              PreparedStatement ps = super.getPreparedStatement(sql);
              ResultSet rs = ps.executeQuery();
              ArrayList<ProdutoDto> Produtos = new ArrayList<>();
              
                while ( rs.next() ){
                    
                         Produtos.add(new ProdutoDto(rs.getInt("idest"), rs.getInt("quantidade")));
                    
                        
                                 
                        
                       
                        }
            
       
            return Produtos;
            
          
            
        } catch (ClassNotFoundException | SQLException e) {
            
        }
         return null;
        
 
    }
    
    
}
