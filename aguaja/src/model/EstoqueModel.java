/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import contratos.IEstoqueService;
import entidades.PedidoDto;
import entidades.ProdutoDto;
import entidades.UsuarioDto;
import entidades.VendedorDto;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mateus
 */
public class EstoqueModel extends DaoUtil implements IEstoqueService {

    
     private String MontaSql( VendedorDto ven){
        String sql = "SELECT idest, marca, litros, preco, quantidade, idven FROM tbestoque ";
        sql += " WHERE 1=1 ";
        
     
        if(ven.getIdUsr() > 0){
            sql += " AND idven = " + ven.getIdUsr();
        }
        
        sql += " order by idest";
        return sql;
    }
    
     
    @Override
    public boolean Adicionar(ProdutoDto prod, VendedorDto vend) {
        
          String sql = "INSERT INTO tbestoque ";
            sql += "(marca, litros, preco, quantidade, idven)";
        sql += " VALUES ";
        sql += "(?, ?, ?, ?, ?)";
        int ret = 0;
        try {
            PreparedStatement ps = super.getPreparedStatement(sql);
            ps.setString(1, prod.getMarca());
            ps.setInt(2, prod.getLitros());
            ps.setDouble(3, prod.getPreco());
            ps.setInt(4, prod.getQuantidade());
            ps.setInt(5, vend.getIdUsr());
            ret = ps.executeUpdate();
            ps.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(EstoqueModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return ret > 0;
    }

    @Override
    public boolean Alterar(ProdutoDto produto) {
         String sql = "UPDATE tbestoque SET ";
        sql += "marca=?, litros=?, preco=?, quantidade=?";
        sql += " WHERE idest=?";

        int ret = 0;
        try {
            PreparedStatement ps = super.getPreparedStatement(sql);
            ps.setString(1, produto.getMarca());
            ps.setInt(2, produto.getLitros() );
            ps.setDouble(3, produto.getPreco());
            ps.setInt(4, produto.getQuantidade());
            ps.setInt(5, produto.getIdproduto());
            ret = ps.executeUpdate();
            ps.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(EstoqueModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return ret > 0;
    }

    @Override
    public boolean Deletar(ProdutoDto prod) {
        String sql = "DELETE FROM tbestoque ";
        sql += " WHERE idest=?";

        int ret = 0;
        try {
            PreparedStatement ps = super.getPreparedStatement(sql);
            ps.setInt(1, prod.getIdproduto());
            ret = ps.executeUpdate();
            ps.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(EstoqueModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return ret > 0;
    }
    

    private ArrayList ListaProdutos(VendedorDto ven){
        ArrayList lstRet = new ArrayList();
        String sql = this.MontaSql( ven );
        try {
            PreparedStatement ps = super.getPreparedStatement(sql);
            ResultSet rs = ps.executeQuery();
            while ( rs.next() ) {
               // if(rs.getInt("quantidade")<=0){ 
                 //   Deletar(new ProdutoDto(rs.getInt("idest"), rs.getInt("quantidade")));
                //}else{
                lstRet.add(
                        new Object[]{
                            rs.getInt("idest"),
                            rs.getString("marca"),
                            rs.getInt("litros"),
                            rs.getDouble("preco"),
                            rs.getInt("quantidade"),
                            
                        }
                );
            }
            //}
            rs.close();
            ps.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(EstoqueModel.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        return lstRet;
    }
    
    @Override
    public ArrayList ListaTodos() {
        return this.ListaProdutos(new VendedorDto() );
    }

    @Override
    public ArrayList ListaTodosFiltrado(VendedorDto vendedor) {
        return this.ListaProdutos(vendedor);
    }
    
    public boolean BaixaEstoque(ProdutoDto produto){
        
         int ret = 0;
       
                if(ConferirEstoque(produto)){
                String sql = "UPDATE tbestoque SET ";
                sql += " quantidade = quantidade - ?";
                sql += " WHERE idest = ?";
                
                 
        try {
            PreparedStatement ps = super.getPreparedStatement(sql);
            ps.setInt(1, produto.getQuantidade());
            ps.setInt(2, produto.getIdproduto());
            ret = ps.executeUpdate();
            ps.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(EstoqueModel.class.getName()).log(Level.SEVERE, null, ex);
          }
        
        
             
                     
         
         }
         return ret > 0 ;
         
    
    }
    
    public boolean ConferirEstoque(ProdutoDto prod){
        
        String sql = "SELECT quantidade FROM tbestoque ";
        sql += " WHERE idest = ?";
        int quantidade = 0;
        try {
            PreparedStatement ps = super.getPreparedStatement(sql);
            ps.setInt(1, prod.getIdproduto());
            ResultSet rs = ps.executeQuery();
            while ( rs.next() ) {
                
                quantidade = rs.getInt("quantidade");
                
                
            }
            rs.close();
            ps.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(EstoqueModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         return prod.getQuantidade()<=quantidade;
        
    }

        
    }   

   

