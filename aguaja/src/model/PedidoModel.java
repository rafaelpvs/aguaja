/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import contratos.IPedidoService;
import entidades.CompradorDto;
import entidades.PedidoDto;
import entidades.ProdutoDto;
import entidades.UsuarioDto;
import entidades.VendedorDto;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mateus
 */
public class PedidoModel extends DaoUtil implements IPedidoService {

    @Override
    public boolean Adicionar(PedidoDto pedido) {
        
        String ped = new String();
        
        for (ProdutoDto prod : pedido.getPedido()) {
            ped+= prod.getMarca() + " " + prod.getLitros() + " Litros " + prod.getQuantidade() + " unidades || ";
        }
            String sql = "INSERT INTO tbpedidos ";
            sql += "(iduser, idven, pedido, valorTotal, situacao)";
            sql += " VALUES ";
            sql += "(?, ?, ?, ?, ?)";
            int ret = 0;
            try {
                PreparedStatement ps = super.getPreparedStatement(sql);
                ps.setInt(1, pedido.getIdcli() );
                ps.setInt(2, pedido.getIdvend());
                ps.setString(3, ped);
                ps.setDouble(4, pedido.getValorTotal());
                ps.setInt(5, pedido.getStatus());
                ret = ps.executeUpdate();
                ps.close();
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(PedidoModel.class.getName()).log(Level.SEVERE, null, ex);
            }

         return ret > 0;
    }
            
    

    @Override
    public boolean Alterar(int idPedido) {
        
        String sql = "UPDATE tbpedidos SET ";
        sql+= " situacao = ? ";
        sql+= " WHERE idped = ?";
        int ret = 0;
            try {
                PreparedStatement ps = super.getPreparedStatement(sql);
                ps.setInt(1,1);
                ps.setInt(2, idPedido);
               
                ret = ps.executeUpdate();
                ps.close();
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(PedidoModel.class.getName()).log(Level.SEVERE, null, ex);
            }

         return ret > 0;
        
    }
    
    public boolean ConfirmaRecebimento(CompradorDto comprador){
           String sql = "UPDATE tbpedidos SET ";
        sql+= " situacao = ? ";
        sql+= " WHERE iduser = ?";
        int ret = 0;
            try {
                PreparedStatement ps = super.getPreparedStatement(sql);
                ps.setInt(1,2);
                ps.setInt(2, comprador.getIdUsr());
               
                ret = ps.executeUpdate();
                ps.close();
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(PedidoModel.class.getName()).log(Level.SEVERE, null, ex);
            }

         return ret > 0;
    }

    @Override
    public boolean Deletar(int idPedido) {
        
        String sql = "DELETE FROM tbpedidos ";
        sql += " WHERE idped =?";

        int ret = 0;
        try {
            PreparedStatement ps = super.getPreparedStatement(sql);
            ps.setInt(1, idPedido);
            ret = ps.executeUpdate();
            ps.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(EstoqueModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return ret > 0;
        
    }

    
     private ArrayList ListaPedidos(UsuarioDto usr, int status, int tipo){
         
        ArrayList lstRet = new ArrayList();
        String sql = "SELECT idped, pedido, valorTotal, idven, iduser FROM tbpedidos ";
        sql += " WHERE 1=1 ";
        if(usr.getIdUsr()!= 0 && tipo == 0){
            sql+= " AND idven = " + usr.getIdUsr();
        }
        if(usr.getIdUsr()!= 0 && tipo == 1){
            sql+= " AND iduser = " + usr.getIdUsr();
        }
        
        if(status==1 || status==0 || status==2)
            sql += " AND situacao = " + status;
        
        sql += " order by idped";
        
        try {
            PreparedStatement ps = super.getPreparedStatement(sql);
            ResultSet rs = ps.executeQuery();
            while ( rs.next() ) {                
                lstRet.add(
                        new Object[]{
                            rs.getInt("idped"),
                            rs.getString("pedido"),
                            rs.getDouble("valorTotal"),
                            rs.getInt("idven"),
                            rs.getInt("iduser"),
                            
                        }
                );
            }
            rs.close();
            ps.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(EstoqueModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return lstRet;
    }
     
    @Override
    public int ListaIdPedido(VendedorDto vendedor, CompradorDto usuario){
        String sql = "SELECT idped FROM tbpedidos ";
        sql+= " WHERE iduser = ?";
        sql+= " AND idven=?";
        sql+=" AND situacao = 0";
        
        try {
            
            PreparedStatement ps = super.getPreparedStatement(sql);
            ps.setInt(1, usuario.getIdUsr());
            ps.setInt(2, vendedor.getIdUsr() );
            ResultSet rs = ps.executeQuery();
            int idpedido;
            while(rs.next()){
                idpedido = rs.getInt("idped");
                return idpedido;
            
        }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(PedidoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
        
        
    }

   @Override
    public ArrayList ListaTodos() {
        return this.ListaPedidos(new VendedorDto(), 3, 2 );
    }

    @Override
    public ArrayList ListaTodosFiltrado(UsuarioDto usuario, int status, int tipo) {
        return this.ListaPedidos(usuario, status, tipo);
    }
    
}
