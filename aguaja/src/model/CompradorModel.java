    package model;


import contratos.IUsuarioService;
import entidades.CompradorDto;
import entidades.UsuarioDto;
import entidades.VendedorDto;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import view.comprador.ProdutosDisponiveisComp;
import view.comprador.SolicitarAguaComp;
import view.comprador.TelaPosLoginComprador;


public class CompradorModel extends DaoUtil implements IUsuarioService{

    public CompradorModel() {
        super();
    }

    @Override
    public boolean Adicionar(UsuarioDto usr) {
        String sql = "INSERT INTO tbclientes ";
        sql += "(nome, email, senha, endereco, cpf, dataNascimento)";
        sql += " VALUES ";
        sql += "(?, ?, ?, ?, ?, ?)";
        int ret = 0;
        try {
            PreparedStatement ps = super.getPreparedStatement(sql);
          
            ps.setString(1, usr.getNome() );
            ps.setString(2, usr.getEmail());
            ps.setString(3, usr.getSenha());
            ps.setString(4, usr.getEndereco());
            ps.setString(5, usr.getCpf());
            ps.setDate(6, new java.sql.Date( usr.getDataNascimento().getTime()));
            ret = ps.executeUpdate();
            ps.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(CompradorModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return ret > 0;
    }

    @Override
    public boolean Alterar(UsuarioDto usr) {
        String sql = "UPDATE tbclientes SET ";
        sql += "nome=?, email=?, senha=?, endereco=?, cpf=?, dataNascimento=?";
        sql += " WHERE iduser=?";

        int ret = 0;
        try {
            PreparedStatement ps = super.getPreparedStatement(sql);
            ps.setString(1, usr.getNome() );
            ps.setString(2, usr.getEmail());
            ps.setString(3, usr.getSenha());
            ps.setString(4, usr.getEndereco());
            ps.setString(5,usr.getCpf());
            ps.setDate(6, (Date) usr.getDataNascimento());
            ps.setInt(7, usr.getIdUsr());
            ret = ps.executeUpdate();
            ps.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(CompradorModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return ret > 0;
    }

    @Override
    public boolean Deletar(UsuarioDto usr) {
        String sql = "DELETE FROM tbclientes ";
        sql += " WHERE iduser=?";

        int ret = 0;
        try {
            PreparedStatement ps = super.getPreparedStatement(sql);
            ps.setInt(1, usr.getIdUsr());
            ret = ps.executeUpdate();
            ps.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(CompradorModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return ret > 0;
    }

    @Override
    public boolean logar(String email, String senha){

             Connection conexao = CompradorModel.conector();
             PreparedStatement pst = null;
             ResultSet rs = null;
            String sql = "select * from tbclientes where email= ? and senha = ?";
            try {
                pst = conexao.prepareStatement(sql);
                pst.setString(1, email);
                pst.setString(2, senha);
                rs = pst.executeQuery();
                if(rs.next()){
                     CompradorDto comp = new CompradorDto(rs.getInt("iduser"),rs.getString("nome"), rs.getString("email"), rs.getString("senha"), rs.getString("endereco"), rs.getString("cpf"), rs.getDate("dataNascimento"));
                     TelaPosLoginComprador tela = new TelaPosLoginComprador(comp); 
                     tela.setVisible(true);
                     conexao.close();
                     return true;


                }else{
                    JOptionPane.showMessageDialog(null, "Usuario e/ou Senha inválido(s)");
                    return false;
            }
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Usuario e/ou Senha inválido(s)");
                    return false;
            }
            
        }

    @Override
            public ArrayList ListarEstabelecimentos(UsuarioDto comp){
                     
                ArrayList lstRet = new ArrayList();
                String sql = "SELECT idven, estabelecimento, endereco FROM tbvendedores";
                sql += " WHERE 1=1 ";
                 if(comp.getEndereco() != null)
                     sql += " AND endereco like '%" + comp.getEndereco() + "%' ";
                 
                sql += " order by idven";
                try {
                  PreparedStatement ps = super.getPreparedStatement(sql);
                  ResultSet rs = ps.executeQuery();
                    while ( rs.next() ) {                
                        lstRet.add(
                               new Object[]{
                                    rs.getInt("idven"),
                                    rs.getString("estabelecimento"),
                                    rs.getString("endereco")
                               }
                        );
                    }
                  
                  rs.close();
                  ps.close();
                } catch (ClassNotFoundException | SQLException ex) {
                  Logger.getLogger(VendedorModel.class.getName()).log(Level.SEVERE, null, ex);
                  }

              return lstRet;
                
                
                
            }

    @Override
    public boolean AdicionarEstabelecimento(VendedorDto usr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override // validação simples de idade, sem levar em condideração mês e dia.
     public boolean validaIdade(java.sql.Date dataN){
            String data = "" + dataN;
            //System.out.println(data);
            String ano = data.substring(0, 4);
            //System.out.println(ano);
            int anoAtual = 2020;
            return anoAtual - Integer.parseInt(ano)>=14;
        }
     
     public double valorFrete(String end1, String end2){
         double frete;
         if(end1.equals(end2))
             return frete = 5.0;
         else
             return frete = 10.0;
     }
}