    package model;


import java.sql.*;

public abstract class DaoUtil {
    
    public static Connection conector(){
        
        java.sql.Connection conexao = null;
        
        //armazenamento de informações referente ao BD 
        String driver = "com.mysql.cj.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/aguaja?useTimezone=true&serverTimezone=UTC";
        String user = "root";
        String psw = "123123";
        try {
            Class.forName(driver);
            conexao = DriverManager.getConnection(url, user, psw);
            return conexao;
        } catch (ClassNotFoundException | SQLException e) {
            //      System.out.println(e);
            return null;
        }
  
    }
    
     public PreparedStatement getPreparedStatement(String sql) throws ClassNotFoundException, 
                                                                     SQLException{
        return DaoUtil.conector().prepareStatement(sql);
    }
    
    public Statement getStatement() throws ClassNotFoundException, SQLException{
        return DaoUtil.conector().createStatement();
    }
    
}